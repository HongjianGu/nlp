#!/usr/bin/env python
# coding: utf-8

import sys, getopt, os
import nltk
import numpy as np
import nltk.tag.hmm as hmm
from nltk.probability import LaplaceProbDist, MLEProbDist, ConditionalFreqDist, ConditionalProbDist
import copy
from sklearn import metrics


VALID_CHAR = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
            'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', ' ', ',', '.']

def main(argv):
    folder = ''
    laplace = False
    lm = False

    if len(argv) == 0:
        print ('decipher.py [-laplace; -lm] <cipher_folder>')
        sys.exit(2)

    if '-h' in argv:
        print ('test.py [-laplace; -lm] <cipher_folder>')
        sys.exit()
    if "-laplace" in argv:
        laplace = True
    if "-lm" in argv:
        lm = True
        
    folder = argv[-1]
    print ('laplace ? ', laplace)
    print ('lm ? ', lm)
    print ('Folder is ', folder)

    files = parse_folder(folder)
    if laplace:
        estimator = lambda fd, bins: LaplaceProbDist(fd, bins)
    else:
        estimator = lambda fd, bins: MLEProbDist(fd)

    predict(estimator, files, lm)

def parser(file):
##  turn the sentence into lower case and filter out characters not in VALID_CHAR
    parsed = []
    for line in file:
        parsed.append(''.join([c for c in line.lower() if c in VALID_CHAR]))
    return parsed

def parse_folder(cipher_folder):
##  call parser() to parse all files in the cipher_folder

    files = ['train_cipher.txt', 'train_plain.txt', 'test_cipher.txt', 'test_plain.txt']

    for file in files:
        fopened = open(os.path.join(cipher_folder, file))
        yield parser(fopened)

def tag(observed, hidden):
##  take a list of observed strings and a list of hidden strings
##  return a list of a list of tuples of the character pairs of every sentence pair in the input lists  

    tagged = []

    for line_observed, line_hidden in zip(observed, hidden):
        tagged.append(zip(list(line_observed), list(line_hidden)))

    return tagged

def predict(estimator, parsed_file, lm = False):
##  decipher the test text
    x_train = next(parsed_file)
    y_train = next(parsed_file)
    x_test = next(parsed_file)
    y_test = next(parsed_file)

##  Training
    trainer = hmm.HiddenMarkovModelTrainer()
##  training data tagged
    train_tagged = tag(x_train, y_train)
##  supervised training
    tagger = trainer.train_supervised(train_tagged, estimator)
    
##  if additional text added to improve
    if lm:
        extra = extra_data()
##  conditional frequence distribution of the extra data
        cfd = ConditionalFreqDist(extra)
##  conditional probability distribution
        cpd = ConditionalProbDist(cfd, estimator, len(VALID_CHAR))
##  get the already trained probabilities
        original_prob = tagger.__getattribute__('_transitions')

        # Add the already trained transition probabilities to the new ones:
        for (cond, prob_dist) in cpd.items():
##            print(prob_dist.freqdist())
##            print(original_prob[cond].freqdist())
            new_fd = prob_dist.freqdist() + original_prob[cond].freqdist()
##            print(new_fd)
            if type(prob_dist) is LaplaceProbDist:
                cpd[cond] = LaplaceProbDist(new_fd, prob_dist._bins)
            elif type(prob_dist) is MLEProbDist:
                cpd[cond] = MLEProbDist(new_fd)

        # Update the model transitions
        tagger.__getattribute__('_transitions').update(cpd)
    
    for i in range(len(x_test)):  # for each sentence in test set
        print ("cipher:\t\t", x_test[i])
        print ("plain:\t\t", y_test[i])
        # predict the real characters based on the list of ciphered characters:
        predictions = tagger.best_path(list(x_test[i]))  # get array of predicted characters
        print ("prediction:\t", ''.join(predictions))

    tests = tag(x_test, y_test)
    test(tests, tagger)

def test(test_sequence, tagger):
##  get the accuracy of the decipher

    def words(sent):
        return [word for (word, tag) in sent]

    def tags(sent):
        return [tag for (word, tag) in sent]

    backup = copy.deepcopy(test_sequence)
    predicted_sequence = [tagger.best_path(line) for line in list(map(words, test_sequence))]
    predicted_sequence = [item for innerlist in predicted_sequence for item in innerlist]

    test_tags = list(map(tags, backup))
    test_tags = [item for innerlist in test_tags for item in innerlist]

    acc = metrics.accuracy_score(test_tags, predicted_sequence)
    count = len(np.unique(test_tags))
    print('accuracy over %d tokens: %.2f' % (count, acc * 100))
    
def extra_data():
##  get more data from internet
    
    tuples = []
    file = []
    extra = open('concat.txt', encoding='utf-8', errors='ignore')
    
##  get rid of extra whitespaces
    for line in extra:
        file.append(" ".join(line.split()))
        
    extra_parsed = parser(file)
    
    for line in extra_parsed:
        ##  append a list of tuples ('char i', 'char i+1')
        tuples.extend(zip(list(line[:-1]), list(line[1:])))

    return tuples



if __name__ == "__main__":
   main(sys.argv[1:])
